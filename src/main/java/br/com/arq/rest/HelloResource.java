package br.com.arq.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import br.com.arq.util.Response;

@Path("/hello")
public class HelloResource {
	
	
	@GET
	@Consumes(MediaType.TEXT_HTML)	
	@Produces(MediaType.TEXT_HTML+";charset=utf-8")	
    public String helloHTML(){
       return  "HTTP <br/>get ";    
    }	
	
	@GET	
	@Path("/json")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)	
    public Response helloJSON(){
		return  Response.OK("Delete JSON");    
    }
    
}
