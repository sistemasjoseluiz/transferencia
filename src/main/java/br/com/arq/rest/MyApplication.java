package br.com.arq.rest;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.core.Application;
import org.glassfish.jersey.media.multipart.MultiPartFeature;


public class MyApplication extends Application{
	@Override
	public Set<Object> getSingletons() {
		Set<Object> singletons = new HashSet<>();
		// Suporte ao File Upload.
		singletons.add(new MultiPartFeature());
		return singletons;
	}
	
    @Override
    public Map<String, Object> getProperties() {
        Map<String, Object> properties = new HashMap<>();
        //Configure o pacote para fazer scan das classes com anotações rest
        properties.put("jersey.config.server.provider.packages","br.com.arq");
        return properties;
    }
}
